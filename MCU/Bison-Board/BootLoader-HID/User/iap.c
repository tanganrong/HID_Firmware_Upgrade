/*
*******************************************************************************************************
*
* 文件名称 : iap.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : IAP文件
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "iap.h"
#include "flash_if.h"


/* 宏定义 -----------------------------------------------------------*/

/* 指令定义 */
#define CONNECT_PC_TO_MCU	0XAA
#define CONNECT_MCU_TO_PC		0XBB
#define FIRMWARE_PC_TO_MCU	0XCC
#define FIRMWARE_MCU_TO_PC	0XDD
#define RECVDONE_MCU_TO_PC	0XEE
#define PROGRAMDONW_MCU_TO_PC	0XFF
#define ERROR_MCU_TO_PC		0X11






/* 变量 -------------------------------------------------------------*/
extern USBD_HandleTypeDef hUsbDeviceFS;

uint8_t USB_Send_Buffer[64] = {0};
uint8_t USB_Recive_Buffer[64] = {0};    //USB接收缓存
uint8_t USB_Received_Count = 0;   //USB接收数据计数
uint32_t Firmware_Count = 0;   //固件接收计数
uint32_t Firmware_Size = 0;   //固件接收长度
uint16_t Firmware_Pack_Length = 0;    //固件的包长度(根据这个判断固件是否发送完成)
int16_t Firmware_Pack_Index = -1;    //固件的包序号(根据这个判断固件是否有遗漏的包)
uint8_t Firmware_Download_Done = 0;  //固件下载完成标志
uint32_t Firmware_Buffer[1024*1024/4] __attribute__((at(0xC0600000)));   //1M固件缓冲区
uint16_t Data_Count = 0;       //接收数据长度


/* 函数声明 ---------------------------------------------------------*/
pFunction JumpToApplication;

extern USBD_StatusTypeDef USBD_DeInit(USBD_HandleTypeDef *pdev);
extern uint8_t USBD_CUSTOM_HID_SendReport(USBD_HandleTypeDef  *pdev, uint8_t *report,uint16_t len);






void iap_schedule(void)
{
	uint8_t Receive_Buffer[64];
	uint8_t Save_Buffer[116];    //一包最大携带58字节固件数据,两包最大116
	uint16_t crc = 0;  //本地计算的CRC值
	uint16_t len = 0;  //长度
	uint8_t hand_shake = 0;		//握手标志位
	uint32_t i =0;
	uint32_t temp = 0;
	uint16_t index = 0;
	uint8_t temp_buf[32];
	
	delay_tim_init();
	LCD_initial();
	fmc_init();
	ltdc_init();
	ltdc_draw_logo();
	MX_USB_DEVICE_Init();
	
	ltdc_clear_layer(LCD_COLOR_BLACK,0);
	
	ltdc_draw_string(0,0,(uint8_t*)"Bison-Board Firmware Download",32,LCD_COLOR_WHITE,LCD_COLOR_BLACK);
	sprintf((char *)temp_buf,"BootLoader version:%s",BOOTLOADER_VERSION);
	ltdc_draw_string(0,32,(uint8_t*)temp_buf,32,LCD_COLOR_WHITE,LCD_COLOR_BLACK);
	ltdc_draw_string(0,64,(uint8_t*)"Please connect the PC with USB",32,LCD_COLOR_WHITE,LCD_COLOR_BLACK);
	ltdc_draw_string(0,96,(uint8_t*)"OPEN RABBIT",32,LCD_COLOR_WHITE,LCD_COLOR_BLACK);
	ltdc_draw_string(0,128,(uint8_t*)"www.whtiaotu.com",32,LCD_COLOR_WHITE,LCD_COLOR_BLACK);

	
	while(1)
	{
		if(USB_Received_Count >= 64)
		{
			Data_Count += USB_Received_Count;
			USB_Received_Count = 0;
			memcpy(Receive_Buffer,USB_Recive_Buffer,64);
			switch(Receive_Buffer[0])
			{
				case CONNECT_PC_TO_MCU:		/* 握手 */
					hand_shake = 0;
					
					HAL_Delay(1);
				
					if(CRC_Check(Receive_Buffer) == 0)
					{
						/* CRC校验通过 */
						
						/* 记录本次固件一共多少个PACK */
						Firmware_Pack_Length = Receive_Buffer[3];
						Firmware_Pack_Length = (Firmware_Pack_Length<<8) + Receive_Buffer[2];
						
						/* 判断固件是否过大 */
						if(Firmware_Pack_Length > 960*1024/58)
						{
							/* 固件过大 */
							/* 向上位机报告错误 */
							for(i=0;i<64;i++)
								USB_Send_Buffer[i] = 0X00;
							USB_Send_Buffer[0] = ERROR_MCU_TO_PC;
							USB_Send_Buffer[1] = 6;
							USB_Send_Buffer[2] = 0;
							USB_Send_Buffer[3] = 0;
							crc = CRC16(USB_Send_Buffer,USB_Send_Buffer[1]-2);
							USB_Send_Buffer[USB_Send_Buffer[1]-2] = crc & 0X00FF;
							USB_Send_Buffer[USB_Send_Buffer[1]-1] = crc >> 8;
							USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS,USB_Send_Buffer,64);
							
							break;
						}
						
						Firmware_Download_Done = 0;
						
						Firmware_Pack_Index = -1;

						USB_Send_Buffer[0] = CONNECT_MCU_TO_PC;
						USB_Send_Buffer[1] = 6;  //长度为6
						USB_Send_Buffer[2] = Receive_Buffer[2];
						USB_Send_Buffer[3] = Receive_Buffer[3];
						crc = CRC16(USB_Send_Buffer,USB_Send_Buffer[1]-2);
						USB_Send_Buffer[USB_Send_Buffer[1]-2] = crc & 0X00FF;
						USB_Send_Buffer[USB_Send_Buffer[1]-1] = crc >> 8;
						//for(i=0;i<64;i++)
							//printf("0X%X ",USB_Send_Buffer[i]);
						hand_shake = 1;    /* 握手标记 */
						Firmware_Count = 0; /* 固件数清0 */
						len = 0;
						memset(Firmware_Buffer,0,sizeof(Firmware_Buffer));  /* 固件内容清0 */
						memset(Save_Buffer,0,sizeof(Save_Buffer));
						
						USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS,USB_Send_Buffer,64); /* 回复 */
						
						ltdc_draw_string(0,160,(uint8_t*)"Download firmware...",32,LCD_COLOR_WHITE,LCD_COLOR_BLACK);
						
					}
				
					break;
				case FIRMWARE_PC_TO_MCU:    /* 传固件 */
					if(hand_shake == 0)
						break;
					memcpy(USB_Send_Buffer,Receive_Buffer,64);
					if(CRC_Check(Receive_Buffer) == 0)
					{
						/* 计算本包序号 */
						index = Receive_Buffer[3];
						index = (index<<8) + Receive_Buffer[2];
						
						/* 确保包是连续的 */
						if(index - Firmware_Pack_Index != 1)
							break;
						Firmware_Pack_Index = index;
						
						len += Receive_Buffer[1]-6;
						
						memcpy(Save_Buffer+(Firmware_Pack_Index%2?58:0),Receive_Buffer+4,Receive_Buffer[1]-6);
						
						/* 存储固件信息,两包存一次 */
						if((Firmware_Pack_Index > 0) && !((Firmware_Pack_Index+1) % 2))
						{
							for(i=0;i<len;i+=4)
							{
								temp = (uint32_t)Save_Buffer[3+i] << 24;
								temp |= (uint32_t)Save_Buffer[2+i] << 16;
								temp |= (uint32_t)Save_Buffer[1+i] << 8;
								temp |= (uint32_t)Save_Buffer[0+i];
								Firmware_Buffer[Firmware_Count] = temp;
								Firmware_Count++;
							}
							len = 0;
						}
						else if((Firmware_Pack_Length % 2) && (Firmware_Pack_Index >= (Firmware_Pack_Length - 1)))
						{
							/* 包长度是奇数且到了最后一包 */
							for(i=0;i<Receive_Buffer[1]-6;i+=4)
							{
								temp = (uint32_t)Save_Buffer[3+i] << 24;
								temp |= (uint32_t)Save_Buffer[2+i] << 16;
								temp |= (uint32_t)Save_Buffer[1+i] << 8;
								temp |= (uint32_t)Save_Buffer[0+i];
								Firmware_Buffer[Firmware_Count] = temp;
								Firmware_Count++;
							}
							printf("Firmware_Count:%d\r\n",Firmware_Count);
							
						}
						
						/* LCD显示下载进度 */
						sprintf((char *)temp_buf,"%d%%",(int)((float)(Firmware_Pack_Index+1)/Firmware_Pack_Length*100));
						ltdc_draw_string(320,160,(uint8_t*)temp_buf,32,LCD_COLOR_WHITE,LCD_COLOR_BLACK);
						
						if(Firmware_Pack_Index < (Firmware_Pack_Length - 1))   /* Firmware_Pack_Index是从0开始的索引 Firmware_Pack_Length是数量 */
						{
							/* 不是最后一包 */
							USB_Send_Buffer[0] = FIRMWARE_MCU_TO_PC;
							crc = CRC16(USB_Send_Buffer,USB_Send_Buffer[1]-2);
							USB_Send_Buffer[USB_Send_Buffer[1]-2] = crc & 0X00FF;
							USB_Send_Buffer[USB_Send_Buffer[1]-1] = crc >> 8;
							USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS,USB_Send_Buffer,64);
						}
						else
						{
							/* 最后一包 */
							USB_Send_Buffer[0] = RECVDONE_MCU_TO_PC;
							crc = CRC16(USB_Send_Buffer,USB_Send_Buffer[1]-2);
							USB_Send_Buffer[USB_Send_Buffer[1]-2] = crc & 0X00FF;
							USB_Send_Buffer[USB_Send_Buffer[1]-1] = crc >> 8;
							USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS,USB_Send_Buffer,64);
							
							Firmware_Download_Done = 1;
						}
						
					}
					break;
				default:
					break;
			}
		}
		else if(Firmware_Download_Done)
		{
			/* 接收固件完成 */
			Firmware_Download_Done = 0;
			if((Firmware_Buffer[0] & 0x2FF00000)  == 0x20000000)   /* 检查栈顶地址是否合法 */
			{
				if((Firmware_Buffer[1] & 0xFF000000)  == 0x08000000)   /* 检查程序存储地址是否合法 */
				{
					ltdc_draw_string(0,192,(uint8_t*)"program...",32,LCD_COLOR_WHITE,LCD_COLOR_BLACK);
					
					printf("start program:%d\r\n",HAL_GetTick());
					
					/* LCD显示正在烧写 */
					flash_if_write(Firmware_Buffer,APPLICATION_ADDRESS,Firmware_Count);
					
					printf("end program:%d\r\n",HAL_GetTick());
					
					/* 向上位机报告,FLASH烧写完成 */
					memset(USB_Send_Buffer,0,sizeof(USB_Send_Buffer));
					USB_Send_Buffer[0] = PROGRAMDONW_MCU_TO_PC;
					USB_Send_Buffer[1] = 6;  //长度为6
					USB_Send_Buffer[2] = 0;
					USB_Send_Buffer[3] = 0;
					crc = CRC16(USB_Send_Buffer,USB_Send_Buffer[1]-2);
					USB_Send_Buffer[USB_Send_Buffer[1]-2] = crc & 0X00FF;
					USB_Send_Buffer[USB_Send_Buffer[1]-1] = crc >> 8;
					USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS,USB_Send_Buffer,64);
					HAL_Delay(50);  /* 这里不延时上位机有很大几率收不到最后一包数据 */
					
					/* 更新完成可以直接跳转 */
					/* Test if user code is programmed starting from address "APPLICATION_ADDRESS" */
					if (((*(__IO uint32_t*)APPLICATION_ADDRESS) & 0x2FF00000 ) == 0x20000000)
					{
						/* 关闭外设 */
						ltdc_delete_layer1();
						ltdc_clear_layer(LCD_COLOR_BLACK,0);
						lcd_backlight(0);
						USBD_DeInit(&hUsbDeviceFS);
						HAL_DeInit();
						HAL_RCC_DeInit();
						
						/* 跳转前关闭用户开启的所有中断 */
						HAL_NVIC_DisableIRQ(TIM4_IRQn);
						HAL_NVIC_DisableIRQ(OTG_FS_IRQn);
						HAL_NVIC_DisableIRQ(SysTick_IRQn);
						
						/* Jump to user application */
						JumpToApplication = (pFunction) *(__IO uint32_t*) (APPLICATION_ADDRESS + 4);
						/* Initialize user application's Stack Pointer */
						__set_MSP(*(__IO uint32_t*) APPLICATION_ADDRESS);
						JumpToApplication();
					}
				}
			}
			
			/* 固件不合格 */
			
			/* 状态清0 */
			hand_shake = 0;
			Firmware_Count = 0;
			Firmware_Pack_Index = -1;
			Firmware_Pack_Length = 0;
			Firmware_Count = 0;
			index = 0;
			len = 0;
			memset(Firmware_Buffer,0,sizeof(Firmware_Buffer));
			memset(Save_Buffer,0,sizeof(Save_Buffer));
			
			/* 向上位机报告错误 */
			HAL_Delay(2);  /* 延时不可少 */
			memset(USB_Send_Buffer,0,sizeof(USB_Send_Buffer));
			USB_Send_Buffer[0] = ERROR_MCU_TO_PC;
			USB_Send_Buffer[1] = 6;
			USB_Send_Buffer[2] = 0;
			USB_Send_Buffer[3] = 0;
			crc = CRC16(USB_Send_Buffer,USB_Send_Buffer[1]-2);
			USB_Send_Buffer[USB_Send_Buffer[1]-2] = crc & 0X00FF;
			USB_Send_Buffer[USB_Send_Buffer[1]-1] = crc >> 8;
			USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS,USB_Send_Buffer,64);
			HAL_Delay(2);
			
		}
	}
}




/**
  * @brief	CRC校验
  * @param	buffer-待校验的数据
  * @retval 0-成功 1-失败
  * @note 针对IAP数据协议的校验,请勿移植
  */
unsigned char CRC_Check(unsigned char *buffer)
{
	uint16_t crc = 0;  //本地计算的CRC值
	uint16_t crc1 = 0; //数据包发来的CRC值
	uint8_t len = 0;  //长度
	
	/* 校验 */
	len = buffer[1];
	crc = CRC16(buffer,len-2);

	crc1 = buffer[len-1];
	crc1 = (crc1<<8) + buffer[len-2];
	if(crc == crc1)
		return 0;
	else
		return 1;
	
}




/**
  * @brief	Calculation CRC
  * @param
  * @retval crc result
  */
unsigned int CRC16 ( unsigned char *arr_buff, unsigned char len)
{

    unsigned int crc=0xFFFF;
    unsigned char i, j;
    for ( j=0; j<len;j++)
    {

        crc=crc ^*arr_buff++;
        for ( i=0; i<8; i++)
        {
            if( ( crc&0x0001) >0)
            {
                crc=crc>>1;
                crc=crc^ 0xa001;
            }
            else
                crc=crc>>1;
        }
    }
    return ( crc);
}




/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
