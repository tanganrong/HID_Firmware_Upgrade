#ifndef WEBBROWSER_H
#define WEBBROWSER_H

#include <QWidget>



namespace Ui {
class WebBrowser;
}

class WebBrowser : public QWidget
{
    Q_OBJECT

public:
    explicit WebBrowser(QWidget *parent = 0);
    ~WebBrowser();

    void set_url(QString s);


private:
    Ui::WebBrowser *ui;

    QString url;
};

#endif // WEBBROWSER_H
