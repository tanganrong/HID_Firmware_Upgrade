#ifndef DIALOG_SET_H
#define DIALOG_SET_H

#include <QDialog>

#include "setting.h"

namespace Ui {
class Dialog_Set;
}

class Dialog_Set : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_Set(QWidget *parent = 0);
    ~Dialog_Set();

private slots:



    void on_pushButton_cancel_clicked();

    void on_pushButton_save_clicked();

    void on_pushButton_default_clicked();

private:
    Ui::Dialog_Set *ui;
};

#endif // DIALOG_SET_H
