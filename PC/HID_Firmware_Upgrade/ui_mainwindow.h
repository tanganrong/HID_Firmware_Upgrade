/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_set;
    QAction *action_help;
    QAction *action_getfirmware;
    QAction *action_postfirmware;
    QWidget *centralWidget;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_constatus_pic;
    QSpacerItem *verticalSpacer;
    QGroupBox *groupBox_info;
    QHBoxLayout *horizontalLayout;
    QLabel *label_url;
    QLabel *label_4;
    QSplitter *splitter;
    QGroupBox *groupBox_set;
    QGridLayout *gridLayout;
    QLabel *label;
    QLabel *label_DevString;
    QLabel *label_5;
    QTextEdit *textEdit_information;
    QLineEdit *lineEdit_FileName;
    QProgressBar *progressBar_Download_Speed;
    QCheckBox *checkBox_loadbin;
    QPushButton *pushButton_OpenFile;
    QPushButton *pushButton_openclose;
    QLabel *label_FirmwareSize;
    QPushButton *pushButton_StartDownload;
    QLabel *label_6;
    QGridLayout *gridLayout_4;
    QLineEdit *lineEdit_vid;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *lineEdit_pid;
    QCheckBox *checkBox_loaddata;
    QGroupBox *groupBox_data;
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *plainTextEdit_receive;
    QPushButton *pushButton_clearReceive;
    QMenuBar *menuBar;
    QMenu *menu;
    QMenu *menu_2;
    QMenu *menu_3;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(637, 632);
        action_set = new QAction(MainWindow);
        action_set->setObjectName(QStringLiteral("action_set"));
        action_help = new QAction(MainWindow);
        action_help->setObjectName(QStringLiteral("action_help"));
        action_getfirmware = new QAction(MainWindow);
        action_getfirmware->setObjectName(QStringLiteral("action_getfirmware"));
        action_postfirmware = new QAction(MainWindow);
        action_postfirmware->setObjectName(QStringLiteral("action_postfirmware"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_3 = new QGridLayout(centralWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(5, 5, 5, 5);
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(10, 10, 10, 0);
        label_constatus_pic = new QLabel(centralWidget);
        label_constatus_pic->setObjectName(QStringLiteral("label_constatus_pic"));
        label_constatus_pic->setMaximumSize(QSize(200, 100));
        label_constatus_pic->setPixmap(QPixmap(QString::fromUtf8(":/img/img/usb_disconnect.png")));

        verticalLayout_2->addWidget(label_constatus_pic);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        gridLayout_2->addLayout(verticalLayout_2, 0, 1, 1, 1);

        groupBox_info = new QGroupBox(centralWidget);
        groupBox_info->setObjectName(QStringLiteral("groupBox_info"));
        horizontalLayout = new QHBoxLayout(groupBox_info);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_url = new QLabel(groupBox_info);
        label_url->setObjectName(QStringLiteral("label_url"));

        horizontalLayout->addWidget(label_url);

        label_4 = new QLabel(groupBox_info);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout->addWidget(label_4);


        gridLayout_2->addWidget(groupBox_info, 1, 0, 1, 2);

        splitter = new QSplitter(centralWidget);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        groupBox_set = new QGroupBox(splitter);
        groupBox_set->setObjectName(QStringLiteral("groupBox_set"));
        gridLayout = new QGridLayout(groupBox_set);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(groupBox_set);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 21, 0, 1, 1);

        label_DevString = new QLabel(groupBox_set);
        label_DevString->setObjectName(QStringLiteral("label_DevString"));

        gridLayout->addWidget(label_DevString, 24, 0, 1, 1);

        label_5 = new QLabel(groupBox_set);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 23, 0, 1, 1);

        textEdit_information = new QTextEdit(groupBox_set);
        textEdit_information->setObjectName(QStringLiteral("textEdit_information"));

        gridLayout->addWidget(textEdit_information, 28, 0, 1, 2);

        lineEdit_FileName = new QLineEdit(groupBox_set);
        lineEdit_FileName->setObjectName(QStringLiteral("lineEdit_FileName"));

        gridLayout->addWidget(lineEdit_FileName, 12, 0, 1, 2);

        progressBar_Download_Speed = new QProgressBar(groupBox_set);
        progressBar_Download_Speed->setObjectName(QStringLiteral("progressBar_Download_Speed"));
        progressBar_Download_Speed->setValue(0);

        gridLayout->addWidget(progressBar_Download_Speed, 22, 0, 1, 2);

        checkBox_loadbin = new QCheckBox(groupBox_set);
        checkBox_loadbin->setObjectName(QStringLiteral("checkBox_loadbin"));

        gridLayout->addWidget(checkBox_loadbin, 16, 0, 1, 2);

        pushButton_OpenFile = new QPushButton(groupBox_set);
        pushButton_OpenFile->setObjectName(QStringLiteral("pushButton_OpenFile"));

        gridLayout->addWidget(pushButton_OpenFile, 18, 0, 1, 2);

        pushButton_openclose = new QPushButton(groupBox_set);
        pushButton_openclose->setObjectName(QStringLiteral("pushButton_openclose"));
        pushButton_openclose->setEnabled(false);

        gridLayout->addWidget(pushButton_openclose, 11, 0, 1, 2);

        label_FirmwareSize = new QLabel(groupBox_set);
        label_FirmwareSize->setObjectName(QStringLiteral("label_FirmwareSize"));

        gridLayout->addWidget(label_FirmwareSize, 20, 0, 1, 1);

        pushButton_StartDownload = new QPushButton(groupBox_set);
        pushButton_StartDownload->setObjectName(QStringLiteral("pushButton_StartDownload"));

        gridLayout->addWidget(pushButton_StartDownload, 19, 0, 1, 2);

        label_6 = new QLabel(groupBox_set);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 25, 0, 1, 1);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        lineEdit_vid = new QLineEdit(groupBox_set);
        lineEdit_vid->setObjectName(QStringLiteral("lineEdit_vid"));
        lineEdit_vid->setEnabled(false);

        gridLayout_4->addWidget(lineEdit_vid, 0, 1, 1, 1);

        label_2 = new QLabel(groupBox_set);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_4->addWidget(label_2, 0, 0, 1, 1);

        label_3 = new QLabel(groupBox_set);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_4->addWidget(label_3, 1, 0, 1, 1);

        lineEdit_pid = new QLineEdit(groupBox_set);
        lineEdit_pid->setObjectName(QStringLiteral("lineEdit_pid"));
        lineEdit_pid->setEnabled(false);

        gridLayout_4->addWidget(lineEdit_pid, 1, 1, 1, 1);


        gridLayout->addLayout(gridLayout_4, 0, 0, 1, 2);

        checkBox_loaddata = new QCheckBox(groupBox_set);
        checkBox_loaddata->setObjectName(QStringLiteral("checkBox_loaddata"));

        gridLayout->addWidget(checkBox_loaddata, 17, 0, 1, 1);

        splitter->addWidget(groupBox_set);
        groupBox_data = new QGroupBox(splitter);
        groupBox_data->setObjectName(QStringLiteral("groupBox_data"));
        verticalLayout = new QVBoxLayout(groupBox_data);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        plainTextEdit_receive = new QPlainTextEdit(groupBox_data);
        plainTextEdit_receive->setObjectName(QStringLiteral("plainTextEdit_receive"));

        verticalLayout->addWidget(plainTextEdit_receive);

        pushButton_clearReceive = new QPushButton(groupBox_data);
        pushButton_clearReceive->setObjectName(QStringLiteral("pushButton_clearReceive"));

        verticalLayout->addWidget(pushButton_clearReceive);

        splitter->addWidget(groupBox_data);

        gridLayout_2->addWidget(splitter, 0, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 637, 23));
        menu = new QMenu(menuBar);
        menu->setObjectName(QStringLiteral("menu"));
        menu_2 = new QMenu(menuBar);
        menu_2->setObjectName(QStringLiteral("menu_2"));
        menu_3 = new QMenu(menuBar);
        menu_3->setObjectName(QStringLiteral("menu_3"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menu->menuAction());
        menuBar->addAction(menu_3->menuAction());
        menuBar->addAction(menu_2->menuAction());
        menu->addAction(action_set);
        menu_2->addAction(action_help);
        menu_3->addAction(action_getfirmware);
        menu_3->addAction(action_postfirmware);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        action_set->setText(QApplication::translate("MainWindow", "\345\217\202\346\225\260\351\205\215\347\275\256", Q_NULLPTR));
        action_help->setText(QApplication::translate("MainWindow", "\345\270\256\345\212\251", Q_NULLPTR));
        action_getfirmware->setText(QApplication::translate("MainWindow", "\350\216\267\345\217\226\345\233\272\344\273\266", Q_NULLPTR));
        action_postfirmware->setText(QApplication::translate("MainWindow", "\344\270\212\344\274\240\345\233\272\344\273\266", Q_NULLPTR));
        label_constatus_pic->setText(QString());
        groupBox_info->setTitle(QApplication::translate("MainWindow", "\350\275\257\344\273\266\344\277\241\346\201\257", Q_NULLPTR));
        label_url->setText(QApplication::translate("MainWindow", "<a href=\"http://www.whtiaotu.com\">\350\267\263\345\205\224\347\247\221\346\212\200 www.whtiaotu.com", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "V0.02\347\250\263\345\256\232 \345\274\200\346\272\220\347\211\210", Q_NULLPTR));
        groupBox_set->setTitle(QApplication::translate("MainWindow", "\346\223\215\344\275\234", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "\344\270\213\350\275\275\350\277\233\345\272\246:", Q_NULLPTR));
        label_DevString->setText(QString());
        label_5->setText(QApplication::translate("MainWindow", "\350\256\276\345\244\207\345\220\215\347\247\260:", Q_NULLPTR));
        checkBox_loadbin->setText(QApplication::translate("MainWindow", "\345\212\240\350\275\275\345\233\272\344\273\266\346\226\207\344\273\266\346\225\260\346\215\256\345\210\260\346\225\260\346\215\256\346\216\245\346\224\266\345\214\272", Q_NULLPTR));
        pushButton_OpenFile->setText(QApplication::translate("MainWindow", "\346\211\223\345\274\200\345\233\272\344\273\266", Q_NULLPTR));
        pushButton_openclose->setText(QApplication::translate("MainWindow", "\346\211\223\345\274\200USB", Q_NULLPTR));
        label_FirmwareSize->setText(QApplication::translate("MainWindow", "\345\233\272\344\273\266\345\244\247\345\260\217:", Q_NULLPTR));
        pushButton_StartDownload->setText(QApplication::translate("MainWindow", "\345\274\200\345\247\213\344\270\213\350\275\275", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "information:", Q_NULLPTR));
        lineEdit_vid->setText(QApplication::translate("MainWindow", "1155", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "vendor_id", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "product_id", Q_NULLPTR));
        lineEdit_pid->setText(QApplication::translate("MainWindow", "22352", Q_NULLPTR));
        checkBox_loaddata->setText(QApplication::translate("MainWindow", "\345\212\240\350\275\275\346\216\245\346\224\266\346\225\260\346\215\256\345\210\260\346\216\245\346\224\266\345\214\272", Q_NULLPTR));
        groupBox_data->setTitle(QApplication::translate("MainWindow", "\346\225\260\346\215\256", Q_NULLPTR));
        pushButton_clearReceive->setText(QApplication::translate("MainWindow", "\346\270\205\351\231\244\346\216\245\346\224\266", Q_NULLPTR));
        menu->setTitle(QApplication::translate("MainWindow", "\351\205\215\347\275\256", Q_NULLPTR));
        menu_2->setTitle(QApplication::translate("MainWindow", "\345\270\256\345\212\251", Q_NULLPTR));
        menu_3->setTitle(QApplication::translate("MainWindow", "\350\216\267\345\217\226\345\233\272\344\273\266", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
