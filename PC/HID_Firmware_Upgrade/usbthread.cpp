#include "usbthread.h"
#include <windows.h>
#include "setting.h"
#include "mainwindow.h"

#include <iostream>

USBThread::USBThread(QObject *parent) : QThread(parent)
{
    status = USB_DISCONNECT;

    usb_write_length = 0;

    thread_stop = false;

    report_id = DEFAULT_REPORT_ID;
    Setting set;
    QString str;
    str = set.readSettings("report_id");
    if(!str.isEmpty())
    {
        report_id = (unsigned char)str.toInt();
    }
    else
    {
        set.writeSettings("report_id",QString::number(report_id));
    }


    hid_init();  // 这一句可要可不要

    tick = QDateTime::currentMSecsSinceEpoch();


}

bool USBThread::open_usb()
{

    status = USB_DISCONNECT;
    usbthread_handle = hid_open(vid,pid,NULL);   //先初始化usbthread_handle,防止后面读取的时候出错
    if(usbthread_handle != NULL)
    {
        qDebug()<<"USB设备打开成功";
        status = USB_CONNECT;

        wchar_t wchar_str[64];
        hid_get_product_string(usbthread_handle,wchar_str,64);
        qDebug()<<QString::fromWCharArray(wchar_str);
        emit usb_connect(QString::fromWCharArray(wchar_str));
        hid_get_serial_number_string(usbthread_handle,wchar_str,64);
        qDebug()<<QString::fromWCharArray(wchar_str);

        return true;
    }
    else
        return false;
}

//继续接收
void USBThread::continue_thread()
{
    thread_pause = false;
}

//暂停接收
void USBThread::pause_thread()
{

    thread_pause = true;
    //关闭USB
    if(status == USB_CONNECT)
        hid_close(usbthread_handle);
    emit usb_disconnect();
    status = USB_DISCONNECT;


}

//停止线程
void USBThread::stop_thread()
{
    thread_stop = true;
}

//发送数据
void USBThread::send_data(unsigned char *data, size_t length)
{

//    if(status == USB_CONNECT)
//    {
//        unsigned char send_buf[65];
//        int i;
//        for(i=0;i<65;i++)
//        {
//            send_buf[i] = i;
//        }
//        send_buf[0] = 0x00;
//        int res = hid_write(usbthread_handle,send_buf,65);
//        qDebug()<<QString::number(res);
//        emit send_num(USB_MAX_DATA_LENGTH);
//    }return;


    if(status != USB_CONNECT)
        return;

    //发送1+64字节数据,下位机接收并处理后64字节
    unsigned char send_buf[USB_MAX_DATA_LENGTH+1] = {0};
    int i=0;
    int res;

    for(i=0;i<USB_MAX_DATA_LENGTH+1;i++)
        send_buf[i] = 0;

    memcpy(send_buf+1,data,length);

    send_buf[0] = report_id;     //第一字节是REPORT ID,固定不变


    res = hid_write(usbthread_handle,send_buf,(size_t)USB_MAX_DATA_LENGTH+1);   //发送REPORT+64字节数据
    if(res < USB_MAX_DATA_LENGTH+1)
    {
        qDebug()<<QString::number(res);
        /* res=-1表示发送失败  res=0表示发送数为0 */
        emit usb_send_error();
        return;
    }
    return;


}

//获取USB连接状态
_USB_STATUS USBThread::get_usbstatus()
{
    return status;
}

//线程主函数
void USBThread::run()
{


    while(!thread_stop)
    {
        if(!thread_pause)
        {
            switch(status)
            {
                case USB_CONNECT:
                {

                    if(hid_read(usbthread_handle,usb_read_data,USB_MAX_DATA_LENGTH) >= USB_MAX_DATA_LENGTH )    //有数据
                    {
                        emit new_data(usb_read_data,USB_MAX_DATA_LENGTH);
                    }
                    else
                    {
                        if(QDateTime::currentMSecsSinceEpoch() - tick < 500 )
                            break;

                        tick = QDateTime::currentMSecsSinceEpoch();

                        wchar_t str[16];
                        int res;
                        res = hid_get_serial_number_string(usbthread_handle,str,16);
                        if(res!=0)
                        {
                           usbthread_handle = hid_open(vid,pid,NULL);
                           status = USB_DISCONNECT;     //标记设备断开
                           emit usb_disconnect();       //发送设备断开的信号
                        }
                    }

                }
                    break;
                case USB_DISCONNECT:
                {
                    /* USB未连接的时候,对实时性要求并不高,避免浪费CPU */
                    if(QDateTime::currentMSecsSinceEpoch() - tick < 500 )
                        break;

                    tick = QDateTime::currentMSecsSinceEpoch();

                    usbthread_handle = hid_open(vid,pid,NULL);
                    if(usbthread_handle != NULL)
                    {
                        qDebug()<<"USB设备打开成功";
                        status = USB_CONNECT;

                        wchar_t wchar_str[64];
                        hid_get_product_string(usbthread_handle,wchar_str,64);
                        qDebug()<<QString::fromWCharArray(wchar_str);
                        emit usb_connect(QString::fromWCharArray(wchar_str));
                    }
                }
                    break;
                default:
                    break;

            }
            /* 未连接状态可以休眠1ms */
            if(status == USB_DISCONNECT)
                msleep(1);
        }
    }
    quit();
}

