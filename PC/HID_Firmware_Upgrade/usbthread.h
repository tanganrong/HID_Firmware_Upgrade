#ifndef USBTHREAD_H
#define USBTHREAD_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QDateTime>

#include "hidapi.h"

#define USB_MAX_DATA_LENGTH 64

typedef enum {USB_CONNECT=0,USB_DISCONNECT}_USB_STATUS;

class USBThread : public QThread
{
    Q_OBJECT



private:
    bool thread_stop;       //true表示停止线程    false表示运行线程
    bool thread_pause;      //true表示暂停线程，即休眠    false表示运行线程
    bool thread_send;       //true表示有待发送的数据

    unsigned char usb_read_data[USB_MAX_DATA_LENGTH];

    unsigned char report_id;

    int usb_write_length;
    unsigned char *usb_write_buf;
    //bool usb_write;

    qint64 tick;


    hid_device *usbthread_handle;



    _USB_STATUS status;

protected:
    void    run() Q_DECL_OVERRIDE;  //线程任务


public:
    explicit USBThread(QObject *parent = nullptr);




    unsigned short vid;
    unsigned short pid;

    void pause_thread();
    void continue_thread();
    void stop_thread();

    bool open_usb();

    void send_data(unsigned char *data,size_t length);       //向USB发送数据

    _USB_STATUS get_usbstatus();

signals:
    void new_data(unsigned char *data,unsigned int length);        //收到新数据

    void usb_connect(QString dev_str);
    void usb_disconnect();
    void usb_send_error();


};

#endif // USBTHREAD_H
