#include "setting.h"

Setting::Setting(QObject *parent):QObject(parent)
{

}



void Setting::writeSettings(const QString &key,const QString &value)
{
    //Qt中使用QSettings类读写ini文件
    //QSettings构造函数的第一个参数是ini文件的路径,第二个参数表示针对ini文件,第三个参数可以缺省
    QSettings *configIniWrite = new QSettings(QDir::currentPath()+"/usb-hid.ini", QSettings::IniFormat);
    //向ini文件中写入内容,setValue函数的两个参数是键值对
    //向ini文件的第一个节写入内容,ip节下的第一个参数
    configIniWrite->setValue(key, value);
    //写入完成后删除指针
    delete configIniWrite;
}



QString Setting::readSettings(const QString &key)
{
    //Qt中使用QSettings类读写ini文件
    //QSettings构造函数的第一个参数是ini文件的路径,第二个参数表示针对ini文件,第三个参数可以缺省
    QSettings *configIniWrite = new QSettings(QDir::currentPath()+"/usb-hid.ini", QSettings::IniFormat);
    //向ini文件中写入内容,setValue函数的两个参数是键值对
    //向ini文件的第一个节写入内容,ip节下的第一个参数
    QString value = configIniWrite->value(key).toString();
    //写入完成后删除指针
    delete configIniWrite;

    return value;


}


void Setting::removeSettings(const QString &key)
{
    //Qt中使用QSettings类读写ini文件
    //QSettings构造函数的第一个参数是ini文件的路径,第二个参数表示针对ini文件,第三个参数可以缺省
    QSettings *configIniWrite = new QSettings(QDir::currentPath()+"/usb-hid.ini", QSettings::IniFormat);
    configIniWrite->remove(key);
    //写入完成后删除指针
    delete configIniWrite;
}
